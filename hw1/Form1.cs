﻿using System;
using System.Windows.Forms;

namespace hw1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // 切換Cheackbox
            rhouse.CheckedChanged += new EventHandler(this.Rhouse_CheckedChanged);
            // 指向共通事件
            ralone.CheckedChanged += new EventHandler(RadioButton_CheckedChanged);
            rnotalone.CheckedChanged += new EventHandler(RadioButton_CheckedChanged);
            rbusiness.CheckedChanged += new EventHandler(RadioButton_CheckedChanged);
            rbusinesshalved.CheckedChanged += new EventHandler(RadioButton_CheckedChanged);
            rnotalone.CheckedChanged += new EventHandler(RadioButton_CheckedChanged);


        }


        // 計算房屋稅率
        void Calculate()
        {
            try 
            {
                int m = Convert.ToInt32(money.Text);
                if (ghouse.Enabled is true)
                {
                    if (ralone.Checked is true)
                        result.Text = (m * 1.2 / 100).ToString();
                    else
                        result.Text = (m * 1.5 / 100).ToString(); 

                }
                else
                {
                    if (rbusiness.Checked is true)
                        result.Text = (m * 3 / 100).ToString();
                    else if (rbusinesshalved.Checked is true)
                        result.Text = (m * 1.5 / 100).ToString();
                    else
                        result.Text = (m * 2 / 100).ToString();
                }

                result.Text += "元";
            }
            catch
            {
                result.Text = "請輸入正確數字";
            }
           
        }

        private void Rhouse_CheckedChanged(object sender, EventArgs e)
        {
            ghouse.Enabled = !ghouse.Enabled;
            gnothouse.Enabled = !gnothouse.Enabled;
            Calculate();
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            Calculate();
        }

        private void Money_TextChanged(object sender, EventArgs e)
        {
            Calculate();
        }
    }
}
