﻿namespace hw1
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.category = new System.Windows.Forms.GroupBox();
            this.rnothouse = new System.Windows.Forms.RadioButton();
            this.rhouse = new System.Windows.Forms.RadioButton();
            this.ghouse = new System.Windows.Forms.GroupBox();
            this.rnotalone = new System.Windows.Forms.RadioButton();
            this.ralone = new System.Windows.Forms.RadioButton();
            this.gnothouse = new System.Windows.Forms.GroupBox();
            this.rallnot = new System.Windows.Forms.RadioButton();
            this.rbusinesshalved = new System.Windows.Forms.RadioButton();
            this.rbusiness = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.money = new System.Windows.Forms.TextBox();
            this.result = new System.Windows.Forms.Label();
            this.category.SuspendLayout();
            this.ghouse.SuspendLayout();
            this.gnothouse.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("新細明體", 15F);
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "課稅現值:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // category
            // 
            this.category.Controls.Add(this.rnothouse);
            this.category.Controls.Add(this.rhouse);
            this.category.Location = new System.Drawing.Point(16, 76);
            this.category.Name = "category";
            this.category.Size = new System.Drawing.Size(102, 96);
            this.category.TabIndex = 2;
            this.category.TabStop = false;
            this.category.Text = "類別";
            // 
            // rnothouse
            // 
            this.rnothouse.AutoSize = true;
            this.rnothouse.Location = new System.Drawing.Point(17, 56);
            this.rnothouse.Name = "rnothouse";
            this.rnothouse.Size = new System.Drawing.Size(59, 16);
            this.rnothouse.TabIndex = 1;
            this.rnothouse.Text = "非住家";
            this.rnothouse.UseVisualStyleBackColor = true;
            // 
            // rhouse
            // 
            this.rhouse.AutoSize = true;
            this.rhouse.Checked = true;
            this.rhouse.Location = new System.Drawing.Point(17, 21);
            this.rhouse.Name = "rhouse";
            this.rhouse.Size = new System.Drawing.Size(47, 16);
            this.rhouse.TabIndex = 0;
            this.rhouse.TabStop = true;
            this.rhouse.Text = "住家";
            this.rhouse.UseVisualStyleBackColor = true;
            // 
            // ghouse
            // 
            this.ghouse.Controls.Add(this.rnotalone);
            this.ghouse.Controls.Add(this.ralone);
            this.ghouse.Location = new System.Drawing.Point(124, 76);
            this.ghouse.Name = "ghouse";
            this.ghouse.Size = new System.Drawing.Size(102, 96);
            this.ghouse.TabIndex = 3;
            this.ghouse.TabStop = false;
            this.ghouse.Text = "住家";
            // 
            // rnotalone
            // 
            this.rnotalone.AutoSize = true;
            this.rnotalone.Location = new System.Drawing.Point(17, 56);
            this.rnotalone.Name = "rnotalone";
            this.rnotalone.Size = new System.Drawing.Size(59, 16);
            this.rnotalone.TabIndex = 1;
            this.rnotalone.Text = "非自住";
            this.rnotalone.UseVisualStyleBackColor = true;
            // 
            // ralone
            // 
            this.ralone.AutoSize = true;
            this.ralone.Checked = true;
            this.ralone.Location = new System.Drawing.Point(17, 21);
            this.ralone.Name = "ralone";
            this.ralone.Size = new System.Drawing.Size(47, 16);
            this.ralone.TabIndex = 0;
            this.ralone.TabStop = true;
            this.ralone.Text = "自住";
            this.ralone.UseVisualStyleBackColor = true;
            // 
            // gnothouse
            // 
            this.gnothouse.Controls.Add(this.rallnot);
            this.gnothouse.Controls.Add(this.rbusinesshalved);
            this.gnothouse.Controls.Add(this.rbusiness);
            this.gnothouse.Enabled = false;
            this.gnothouse.Location = new System.Drawing.Point(238, 76);
            this.gnothouse.Name = "gnothouse";
            this.gnothouse.Size = new System.Drawing.Size(102, 96);
            this.gnothouse.TabIndex = 4;
            this.gnothouse.TabStop = false;
            this.gnothouse.Text = "非住家";
            // 
            // rallnot
            // 
            this.rallnot.AutoSize = true;
            this.rallnot.Location = new System.Drawing.Point(17, 65);
            this.rallnot.Name = "rallnot";
            this.rallnot.Size = new System.Drawing.Size(71, 16);
            this.rallnot.TabIndex = 2;
            this.rallnot.Text = "非住非營";
            this.rallnot.UseVisualStyleBackColor = true;
            // 
            // rbusinesshalved
            // 
            this.rbusinesshalved.AutoSize = true;
            this.rbusinesshalved.Location = new System.Drawing.Point(17, 43);
            this.rbusinesshalved.Name = "rbusinesshalved";
            this.rbusinesshalved.Size = new System.Drawing.Size(71, 16);
            this.rbusinesshalved.TabIndex = 1;
            this.rbusinesshalved.Text = "營業減半";
            this.rbusinesshalved.UseVisualStyleBackColor = true;
            // 
            // rbusiness
            // 
            this.rbusiness.AutoSize = true;
            this.rbusiness.Checked = true;
            this.rbusiness.Location = new System.Drawing.Point(17, 21);
            this.rbusiness.Name = "rbusiness";
            this.rbusiness.Size = new System.Drawing.Size(47, 16);
            this.rbusiness.TabIndex = 0;
            this.rbusiness.TabStop = true;
            this.rbusiness.Text = "營業";
            this.rbusiness.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("新細明體", 15F);
            this.label2.Location = new System.Drawing.Point(9, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "應繳金額:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // money
            // 
            this.money.Font = new System.Drawing.Font("新細明體", 10F);
            this.money.Location = new System.Drawing.Point(118, 40);
            this.money.Name = "money";
            this.money.Size = new System.Drawing.Size(100, 23);
            this.money.TabIndex = 1;
            this.money.Text = "0";
            this.money.TextChanged += new System.EventHandler(this.Money_TextChanged);
            // 
            // result
            // 
            this.result.Font = new System.Drawing.Font("新細明體", 15F);
            this.result.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.result.Location = new System.Drawing.Point(114, 187);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(188, 23);
            this.result.TabIndex = 6;
            this.result.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(352, 226);
            this.Controls.Add(this.result);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gnothouse);
            this.Controls.Add(this.ghouse);
            this.Controls.Add(this.category);
            this.Controls.Add(this.money);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "房屋稅";
            this.category.ResumeLayout(false);
            this.category.PerformLayout();
            this.ghouse.ResumeLayout(false);
            this.ghouse.PerformLayout();
            this.gnothouse.ResumeLayout(false);
            this.gnothouse.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox category;
        private System.Windows.Forms.RadioButton rnothouse;
        private System.Windows.Forms.RadioButton rhouse;
        private System.Windows.Forms.GroupBox ghouse;
        private System.Windows.Forms.RadioButton rnotalone;
        private System.Windows.Forms.RadioButton ralone;
        private System.Windows.Forms.GroupBox gnothouse;
        private System.Windows.Forms.RadioButton rallnot;
        private System.Windows.Forms.RadioButton rbusinesshalved;
        private System.Windows.Forms.RadioButton rbusiness;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox money;
        private System.Windows.Forms.Label result;
    }
}

